<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

// do not use "use" for \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider due to TYPO3 caching bug!

// https://wiki.typo3.org/TYPO3.CMS/Releases/7.5/Feature#Feature:_.2368741_-_Introduce_new_IconFactory_as_base_to_replace_the_icon_skinning_API
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$icons = [
    'drk-ce-contact-person' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Contact.svg',
    'drk-ce-ambassador' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Ambassador.svg',
    'drk-ce-teamleader' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_TeamLeader.svg',
    'drk-ce-stage' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Stage.svg',
    'drk-ce-blooddonation' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_BloodDonation.svg',
    'drk-ce-hotline' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Hotline.svg',
    'drk-ce-mediathek' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Mediathek.svg',
    'drk-ce-doctorsearch' => 'EXT:drk_template2016/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_DoctorSearch.svg'
];

foreach ($icons as $iconKey => $iconPath) {
    $iconRegistry->registerIcon(
        $iconKey,
        \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        [
            'source' => $iconPath
        ]
    );
}


/**
 * Add custom page type 178 for menus
 */
call_user_func(
    function ($extKey, $table, $dokType, $pageIcon) {
        \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class)
            ->registerIcon(
                'apps-pagetree-' . $dokType,
                TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                [
                    'source' => 'EXT:' . $extKey . '/' . $pageIcon,
                ]
            );

        $pageRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\DataHandling\PageDoktypeRegistry::class);
        $pageRegistry->add(
            $dokType,
            [

            ]
        );

        // Allow backend users to drag and drop the new page type:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
            'options.pageTree.doktypesToShowInNewPageDragArea := addToList(' . $dokType . ')'
        );
    },
    'drk_template2016',
    'pages',
    178,
    'Resources/Public/Icons/Pagetype/apps-pagetree-178.svg'
);

