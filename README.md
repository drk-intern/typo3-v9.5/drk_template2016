Funktion
=====

Menu
-----

Für das Menu wird die folgende Seitenstruktur verwendet:
 
```
Start
  Angebote
    Spalte 1                     -> Seitentyp: Ordner
      Gesundheit und Prävention
```

Die Spalten werden genutzt um die Seiten im Menu auf dem Desktop zu sortieren.
 
Wenn eine Seite vom Typ "Menuspalten-Trenner" ist, so ist diese nicht mehr in der Rootline sichtbar. 

Logo Einstellungen
-----

Zum Ändern des *Brands* geht man wie folgt vor:

1. öffnen des TYPO3 Backends
2. Modul `Web > Template`
3. Auswählen der Seite `Startseite` mit der `ID=1`
4. In der Selectbox den *Konstanten-Editor* wählen
5. Kategorie `Logo` wählen
6. Einstellungen treffen


Sucheinstellungen
-----

Zum Ändern der *Sucheinstellungen* geht man wie folgt vor:

1. öffnen des TYPO3 Backends
2. Modul `Web > Template`
3. Auswählen der Seite `Startseite` mit der `ID=1`
4. In der Selectbox den *Konstanten-Editor* wählen
5. Kategorie `Suche` wählen
6. Einstellungen treffen

Änderungen
-----

Repositories:

```
Primär:   https://git.4viewture.eu/DRK-Service-GmbH/drk_template2016
                  |
                  |  automatischer push via Gitlab CI
                 ...
                  .
Secundär: http://drk010.bamsenet.de/typo3/drk_template2016

```

Menu
-----

Das Menu braucht eine spezielle Struktur

```
Ebene 1 (Angebote)
    Ebene 2 (Spalte 1) - nicht sichtbar gibt die Spalte an
        Ebene 3 (Behindertenhilfe) - Blocküberschrift
            Ebene 4 (...) - Blockeintrag 
```

Dokumentation
-----

* https://docs.google.com/presentation/d/1QXjoR-VjO4p5HeGb4uV_uvhwo7q3nRuoFstyfRWZ5Xk/edit?usp=sharing

Buildscript
-----

Unter `Resources/Privat/Buildscripts` ist das Buildscript abgelegt.

Zur Ausführung wird `grunt` benötigt.

Folgende Prozesse sind aktuell nutzbar:

* `grunt image` -> Erzeugt Spritmap und CSS neu
* `grunt scss` -> Erzeugt CSS neu
* `grunt js` -> Erzeugt JS neu


Fixing and Setting up sass and scss on OSX
-----

```
sudo gem install -n /usr/local/bin sass 
sudo gem install -n /usr/local/bin scss_lint
sudo npm rebuild node-sass
```

Setting up Build Env in VagrantBox (Debian)
-----

Alle Kommandos als root ...

```
gem update --system && gem install scss-lint
sudo apt-get install nodejs-legacy
apt-get install npm
npm rebuild node-sass
npm install -g grunt-cli
```