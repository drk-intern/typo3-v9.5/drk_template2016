#
# Table structure for table 'tt_content'
#

CREATE TABLE tt_content (
    fe_users_records text,
);

#
# Table structure for table 'pages'
#
CREATE TABLE pages (
	show_social_media_shares tinyint(4) DEFAULT '1' NOT NULL,
	show_print_pdf_buttons tinyint(4) DEFAULT '0' NOT NULL
);

#
# Table structure for table 'fe_users'
#
CREATE TABLE fe_users (
	description text,
	teamlink_pid int(11) NOT NULL DEFAULT '0',
	teamlink_title varchar(255) DEFAULT '' NOT NULL,
	drk_user_image varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (
	drk_show_copyright tinyint(4) DEFAULT '0' NOT NULL
);

#
# Table structure for table 'tx_news_domain_model_news'
#
CREATE TABLE tx_news_domain_model_news (
    is_event TINYINT(1) unsigned DEFAULT '0' NOT NULL,
    datetime_end INT(11) unsigned DEFAULT '0',
);

#
# Table structure for table 'be_groups'
#
# overwrite category_perms field due to size restriction of default varchar(255) field.
CREATE TABLE be_groups (
	category_perms text
);

# https://blog.marit.ag/2015/06/30/typo3-und-fal-override-felder-in-irre-elementen-aendern/
#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (
    caption tinytext,
);
