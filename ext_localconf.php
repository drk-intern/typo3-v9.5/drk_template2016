<?php
defined('TYPO3') or die();

/**
 * Bugfix for https://forge.typo3.org/issues/58606 keep it here and not in Overides!
 */

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('
    <INCLUDE_TYPOSCRIPT: source="FILE:EXT:drk_template2016/Configuration/PageTS/PageTSconfig.typoscript">
');

/**
 * Presets
 */
call_user_func(static function() {
    \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
        $GLOBALS['TYPO3_CONF_VARS'],
        [
            'BE' => [
                'interfaces' => 'backend, frontend'
            ],
            'EXT' => [
                'extConf' => [
                    'extractor' => 'a:11:{s:11:"enable_tika";s:1:"0";s:21:"enable_tools_exiftool";s:1:"1";s:20:"enable_tools_pdfinfo";s:1:"0";s:10:"enable_php";s:1:"1";s:9:"tika_mode";s:3:"jar";s:13:"tika_jar_path";s:0:"";s:16:"tika_server_host";s:0:"";s:16:"tika_server_port";s:4:"9998";s:14:"tools_exiftool";s:17:"/usr/bin/exiftool";s:13:"tools_pdfinfo";s:0:"";s:22:"mapping_base_directory";s:54:"EXT:drk_template2016/Configuration/Extractor/Services/";}'
                ],
                'news' => [
                    'classes' => [
                        'Domain/Model/News' => [
                            'drk_template2016' => 'drk_template2016'
                        ]
                    ]
                ]
            ],
            'EXTENSIONS' => [
                'backend' => [
                    'backendFavicon' => 'EXT:drk_template2016/Resources/Public/backend/favicon.ico',
                    'loginBackgroundImage' => 'EXT:drk_template2016/Resources/Public/Backend/Background.png',
                    'loginFootnote' => 'DRK Service GmbH / 4viewture GmbH',
                    'loginHighlightColor' => '#53abe1',
                    'loginLogo' => 'EXT:drk_template2016/Resources/Public/Backend/DRK_Logo.svg',
                ]
            ],
            'RTE' => [
                'Presets' => [
                    'custom_preset' => 'EXT:drk_template2016/Configuration/RTE/custom.yaml'
                ]
            ],
            'SYS' => [
                'locallangXMLOverride' => [
                    'de' => [
                        'EXT:news/Resources/Private/Language/locallang_modadministration.xlf' => [
                            'drk_template2016' =>
                                'EXT:drk_template2016/Resources/Private/Language/Overrides/de.locallang_news_modadministration.xlf'
                        ]
                    ]
                ]
            ]
        ]
    );
});
