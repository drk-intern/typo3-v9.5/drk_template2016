<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    static function () {
        $langFilePrefix =
            'LLL:EXT:drk_template2016/Resources/Private/Language/locallang_be.xlf:';

        $temporaryColumns = [
            'show_social_media_shares' => [
                'exclude' => 1,
                'label' => $langFilePrefix . 'pages.show_social_media_shares',
                'config' => [
                    'type' => 'check',
                    'renderType' => 'checkboxToggle',
                    'items' => [
                        [
                            '0' => $langFilePrefix . 'pages.show_social_media_shares.enabled',
                            '1' => '',
                        ]
                    ]
                ]
            ],
        ];

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $temporaryColumns);

        // add show_social_media_shares field to miscellaneous palette
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
            'pages',
            'show_social_media_shares',
            '',
            'after:is_siteroot'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette(
            'pages',
            'title',
            '--linebreak--,teaser_text,--linebreak--',
            'after:subtitle'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            'tt_content',
            'CType',
            array('Weitere Inhaltselemente', '--div--')
        );
    }
);

/**
 * Add custom page type 178 for menus
 */
call_user_func(
    function ($extKey, $table, $dokType, $pageIcon) {

        // Add new page type as possible select item:
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTcaSelectItem(
            $table,
            'doktype',
            [
                'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang.xlf:menu_page_type',
                $dokType,
                'EXT:' . $extKey . '/' . $pageIcon,
            ],
            '1',
            'after'
        );

        // Add icon for new page type:
        \TYPO3\CMS\Core\Utility\ArrayUtility::mergeRecursiveWithOverrule(
            $GLOBALS['TCA']['pages'],
            [
                'ctrl' => [
                    'typeicon_classes' => [
                        $dokType => 'apps-pagetree-' . $dokType,
                    ],
                ],
            ]
        );
    },
    'drk_template2016',
    'pages',
    178,
    'Resources/Public/Icons/Pagetype/apps-pagetree-178.svg'
);
