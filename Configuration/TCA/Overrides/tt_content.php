<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(function () {
    $languageFilePrefix = 'LLL:EXT:drk_template2016/Resources/Private/Language/locallang_be.xlf:';
    $frontendLanguageFilePrefix = 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:';

    // register this plugin
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'drk_template2016',
        'DrkTemplate',
        'DRK Template'
    );

    // add default TypoScript code
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
        'drk_template2016',
        'Configuration/TypoScript',
        'DRK Template'
    );

    // Configure a customized header without a date field
    $GLOBALS['TCA']['tt_content']['palettes']['drk_header'] = array(
        'showitem' => '
        header;' . $frontendLanguageFilePrefix . 'header_formlabel,
        --linebreak--,
        header_layout;' . $frontendLanguageFilePrefix . 'header_layout_formlabel,
        --linebreak--,
        header_link;' . $frontendLanguageFilePrefix . 'header_link_formlabel
    ',
    );

    // Configure content element "Text & Media"
    $GLOBALS['TCA']['tt_content']['types']['textmedia']['showitem'] = '
    --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
    --palette--;' . $frontendLanguageFilePrefix . 'palette.header;drk_header,rowDescription,
    bodytext;' . $frontendLanguageFilePrefix . 'bodytext_formlabel,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.media,
    assets,
    --palette--;' . $frontendLanguageFilePrefix . 'palette.imagelinks;imagelinks,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.appearance,
    --palette--;' . $frontendLanguageFilePrefix . 'tt_content.palette.mediaAdjustments;mediaAdjustments,
    --palette--;' . $frontendLanguageFilePrefix . 'tt_content.palette.gallerySettings;gallerySettings,
    --palette--;' . $frontendLanguageFilePrefix . 'palette.appearanceLinks;appearanceLinks,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
    hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden,
    --palette--;' . $frontendLanguageFilePrefix . 'palette.access;access,
    --div--;' . $frontendLanguageFilePrefix . 'tabs.extended
    ';

    // Configure content element header
    $GLOBALS['TCA']['tt_content']['types']['header']['showitem'] = '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.header;drk_header,rowDescription,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
            hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.access;access,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.extended
    ';

    // Configure content element blood donation
    $GLOBALS['TCA']['tt_content']['types']['drk_drk_blood_donation'] = array(
        'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
        header;' . $frontendLanguageFilePrefix . 'header_formlabel,
    ',
    );

    // Configure content element "blood donation"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.blood_donation.title',
            'drk_drk_blood_donation',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_BloodDonation.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure content element doctor search
    $GLOBALS['TCA']['tt_content']['types']['drk_drk_doctor_search'] = array(
        'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
        header;' . $frontendLanguageFilePrefix . 'header_formlabel,
    ',
    );

    // Configure content element "doctor search"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.doctor_search.title',
            'drk_drk_doctor_search',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_DoctorSearch.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure content element "Stage image"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.stage_image.title',
            'drk_template2016_stage',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Stage.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure the default backend fields for the content element "Stage image"
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_stage'] = array(
        'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
        header;' . $frontendLanguageFilePrefix . 'header_formlabel,
        --linebreak--,
        date;' . $frontendLanguageFilePrefix . 'date_formlabel,
        header_link;' . $frontendLanguageFilePrefix . 'header_link_formlabel,
        rowDescription,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.appearance,
        layout,
        --div--;' . $languageFilePrefix . 'tabs.media_buehne,assets'
    );

    // add content element "contact person
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.contact_person.title',
            'drk_template2016_contact_person',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Contact.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure the default backend fields for the content element "Contact Person"
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_contact_person'] = array(
        'showitem' => '
       --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
       header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
       --linebreak--,
       header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
       --linebreak--,
       rowDescription, fe_users_records;' . $frontendLanguageFilePrefix . 'records.ALT.menu_formlabel'
    );

    // configure usage of fe_user_records maxitems to keep the limit of
    // selected users
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_contact_person']['columnsOverrides'] = array(
        'fe_users_records' => array(
            'label' => 'Recipients:',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'foreign_table' => 'fe_users',
                'allowed' => 'fe_users',
                'size' => 10,
                'maxitems' => 20,
                'minitems' => 1,
                'show_thumbs' => 10
            )
        ),
    );

    // add content element "ambassador"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.ambassador.title',
            'drk_template2016_ambassador',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Ambassador.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure the default backend fields for the content element "ambassador"
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_ambassador'] = array(
        'showitem' => '
       --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
       header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
       --linebreak--,
       header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
       --linebreak--,bodytext,
       rowDescription, fe_users_records;' . $frontendLanguageFilePrefix . 'records.ALT.menu_formlabel'
    );

    // configure usage of RTE bodytext field inside content element ambassador
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_ambassador']['columnsOverrides'] = array(
        'bodytext' => array(
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        ),
        'fe_users_records' => array(
            'config' => array(
                'size' => 10,
                'maxitems' => 25,
                'minitems' => 1
            )
        )
    );

    // add content element "team leader"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.teamleader.title',
            'drk_template2016_teamleader',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_TeamLeader.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure the default backend fields for the content element "team leader"
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_teamleader'] = array(
        'showitem' => '
       --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
       header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
       --linebreak--,
       header_layout;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_layout_formlabel,
       --linebreak--,bodytext,
       rowDescription, fe_users_records;' . $frontendLanguageFilePrefix . 'records.ALT.menu_formlabel'
    );

    // configure usage of RTE bodytext field inside content element ambassador
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_teamleader']['columnsOverrides'] = array(
        'bodytext' => array(
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        ),
        'fe_users_records' => array(
            'config' => array(
                'size' => 10,
                'maxitems' => 25,
                'minitems' => 1
            )
        )
    );

    // add template element "hotline"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.hotline.title',
            'drk_template2016_hotline',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Hotline.svg'
        ),
        'CType',
        'drk_template2016'
    );

    // Configure the default backend fields for the template element "hotline"
    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_hotline'] = array(
        'showitem' => '
       --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
       header;' . $frontendLanguageFilePrefix . 'header_formlabel,
       --linebreak--,
       header_layout;' . $frontendLanguageFilePrefix . 'header_layout_formlabel'
    );

    $GLOBALS['TCA']['tt_content']['types']['menu'] = array(
        'showitem' => '
        --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.header;header,rowDescription,
            bodytext,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.menu;menu,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.appearance,
            layout;' . $frontendLanguageFilePrefix . 'layout_formlabel,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.appearanceLinks;appearanceLinks,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.access,
            hidden;' . $frontendLanguageFilePrefix . 'field.default.hidden,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.access;access,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.accessibility,
            --palette--;' . $frontendLanguageFilePrefix . 'palette.menu_accessibility;menu_accessibility,
        --div--;' . $frontendLanguageFilePrefix . 'tabs.extended
        ',
        'subtype_value_field' => 'menu_type',
    );

    // configure usage of RTE bodytext field inside content element "Menu"
    $GLOBALS['TCA']['tt_content']['types']['menu']['columnsOverrides'] = array(
        'bodytext' => array(
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        )
    );

    // add content element "Mediathek"
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPlugin(
        array(
            $languageFilePrefix . 'content_element.mediathek.title',
            'drk_template2016_mediathek',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('drk_template2016')
            .
            'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Mediathek.svg'
        ),
        'CType',
        'drk_template2016'
    );

    $GLOBALS['TCA']['tt_content']['types']['drk_template2016_mediathek'] = array(
        'showitem' => '
       --palette--;' . $frontendLanguageFilePrefix . 'palette.general;general,
       header;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:header_formlabel,
       --linebreak--,
       rowDescription, assets;' . $languageFilePrefix . 'content_element.mediathek.media,'
    );

    // configure usage of RTE bodytext field inside content element "Menu"
    $GLOBALS['TCA']['tt_content']['types']['menu']['columnsOverrides'] = array(
        'bodytext' => array(
            'defaultExtras' => 'richtext:rte_transform[mode=ts_css]'
        )
    );

    // setup new field for fe_user relation in tt_content elements
    $tempColumns = array(
        'fe_users_records' => array(
            'label' => 'Recipients:',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'foreign_table' => 'fe_users',
                'allowed' => 'fe_users',
                'size' => 10,
                'maxitems' => 25,
                'minitems' => 1,
                'show_thumbs' => 1
            )
        ),
    );

    // add field to tca
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'tt_content',
        $tempColumns
    );
});
