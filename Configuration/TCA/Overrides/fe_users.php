<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(function () {
    $langFilePrefix = 'LLL:EXT:drk_template2016/Resources/Private/Language/locallang_be.xlf';
    $langFile = 'LLL:EXT:lang/locallang_tca.xlf:';

    // configure database field to inline relation
    $tempColumnsTtContent = array(
        'description' => array(
            'label' => $langFilePrefix . ':fe_users.description',
            'exclude' => 1,
            'defaultExtras' => 'richtext[]',
            'config' => array(
                'type' => 'text',
            )
        ),
        'teamlink_pid' => array(
            'label' => $langFilePrefix . ':fe_users.teamlink_pid',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'foreign_table' => 'pages',
                'default' => 0,
                'allowed' => 'pages',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 0,
                'show_thumbs' => 1
            )
        ),
        'teamlink_title' => array(
            'label' => $langFilePrefix . ':fe_users.teamlink_title',
            'exclude' => 1,
            'config' => array(
                'type' => 'input',
                'size' => '50',
                'max' => '255',
                'eval' => 'trim'
            )
        ),
        'drk_user_image' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . ':fe_users.drk_user_image',
            'config' => [
                'type' => 'file',
                // Use the imageoverlayPalette instead of the basicoverlayPalette
                'foreign_match_fields' => array(
                    'fieldname' => 'drk_user_image'
                ),
                'foreign_types' => array(
                    '0' => array(
                        'showitem' => '
							--palette--;' . $langFile . 'sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    ),
                    \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => array(
                        'showitem' => '
							--palette--;' . $langFile . 'sys_file_reference.imageoverlayPalette;imageoverlayPalette,
							--palette--;;filePalette'
                    )
                ),
                'minitems' => 0,
                'maxitems' => 1,
                'allowes' => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ]
        )
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
        'fe_users',
        $tempColumnsTtContent
    );

    $GLOBALS['TCA']['fe_users']['types'] = array(
        '0' => array(
            'showitem' => '
				disable, username, password, usergroup, lastlogin,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.personelData,
				company, --palette--;;1, name, --palette--;;2, address, zip, city, country, telephone, fax, email, www,
				drk_user_image, description, teamlink_pid, teamlink_title,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.options,
				lockToDomain, TSconfig,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.access,
				starttime, endtime,
				--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_tca.xlf:fe_users.tabs.extended
			',
        ),
    );
});
