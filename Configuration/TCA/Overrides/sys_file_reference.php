<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(static function () {
    // set image manipulation ratios for image cropping in BE
    $GLOBALS['TCA']['sys_file_reference']['columns']['crop']['config'] = [
        'type' => 'imageManipulation',
        'cropVariants' => [
            'default' => [
                'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.crop_variant.default',
                'selectedRatio' => 'NaN',
                'allowedAspectRatios' => [
                    'NaN' => [
                        'title' => 'LLL:EXT:core/Resources/Private/Language/locallang_wizards.xlf:imwizard.ratio.free',
                        'value' => 0.0
                    ],
                    '4:5' => [
                        'title' => '4:5',
                        'value' => 4/5,
                    ],
                    '1:1' => [
                        'title' => '1:1',
                        'value' => 1/1,
                    ],
                    '5:4' => [
                        'title' => '5:4',
                        'value' => 5/4,
                    ],
                    '4:3' => [
                        'title' => '4:3',
                        'value' => 4/3,
                    ],
                    '3:1' => [
                        'title' => '3:1',
                        'value' => 3/1,
                    ],
                ],
            ]
        ]
    ];

    $tempColumns = array(
        'caption' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:drk_template2016/Resources/Private/Language/locallang_be.xlf:caption',
            'config' => array(
                'default' => '',
                'eval' => 'null',
                'mode' => 'useOrOverridePlaceholder',
                'placeholder' => '__row|uid_local|metadata|caption',
                'size' => 20,
                'type' => 'input'
            )
        )
    );
    TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_file_reference',$tempColumns,1);

    TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addFieldsToPalette('sys_file_reference', 'imageoverlayPalette','caption');
});
