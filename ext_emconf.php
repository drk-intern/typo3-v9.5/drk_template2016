<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
/***************************************************************
 * Extension Manager/Repository config file for ext: 'drk_template2016'
 *
 * Auto generated by Extension Builder 2015-05-22
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * 'version' and 'dependencies' must not be touched!
 ***************************************************************/

$EM_CONF['drk_template2016'] = array(
    'title' => 'DRK Template 2016',
    'description' => 'Template for DRK organisation websites 2016',
    'category' => 'templates',
    'author' => 'Deutsches Rotes Kreuz - Dienstleistungs- und Database-Marketing (OE 35)',
    'author_email' => 'ungeheuh@drk.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '12.10.0',
    'constraints' => array(
        'depends' => array(
            'typo3' => '10.4.0-10.4.99',
            'news' => '11.0.0-11.99.99'
        ),
        'conflicts' => array(),
    ),
    'autoload' => array(
        'psr-4' => array(
            'Drk\\DrkTemplate\\' => 'Classes/'
        )
    )
);
