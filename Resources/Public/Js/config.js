/**
 * Require config
 */
var require = {
    baseUrl: '/typo3conf/ext/drk_template2016/Resources/Public/Js/',
    paths: {
        'jquery': '../Vendor/jquery/dist/jquery-2.2.4.min',

        'fancybox': '../Vendor/fancybox/source/jquery.fancybox',
        'fastclick': '../Vendor/fastclick/lib/fastclick',
        'jquery.drilldown': '../Vendor/jquery-drilldown/dist/jquery.drilldown',
        'jquery.cookie': '../Vendor/jquery.cookie/jquery.cookie',
        'jquery.multiselect': '../Vendor/jQuery-MultiSelect/jquery.multiselect',
        'jquery.transit': '../Vendor/jquery.transit/jquery.transit',
        'jquery.maskinput': '../Vendor/jquery.maskinput/dist/jquery.maskedinput',
        'jquery.validate': '../Vendor/jquery.validate/dist/jquery.validate',
        'jquery.visible': '../Vendor/jquery-visible/jquery.visible',
        'lightgallery': '../Vendor/lightgallery/dist/js/lightgallery-all',
        'mediaelement': '../Vendor/mediaelement/build/mediaelement-and-player',
        'modernizr': 'build/modernizr.custom',
        'moment': '../Vendor/moment/min/moment-with-locales.min',

        'pikaday': '../Vendor/pikaday/pikaday',
        'jquery.pikaday': '../Vendor/pikaday/plugins/pikaday.jquery',
        'placeholder': '../Vendor/jquery-placeholder/jquery.placeholder',
        'slick': '../Vendor/slick.js/slick/slick',
        'svg4everybody': '../Vendor/svg4everybody/dist/svg4everybody',
        'parsley': '../Vendor/parsleyjs/dist/parsley',

        /* Foundation */
        'foundation.core': '../Vendor/foundation/js/foundation/foundation',
        'foundation.abide': '../Vendor/foundation/js/foundation/foundation.abide',
        'foundation.accordion': '../Vendor/foundation/js/foundation/foundation.accordion',
        'foundation.alert': '../Vendor/foundation/js/foundation/foundation.alert',
        'foundation.clearing': '../Vendor/foundation/js/foundation/foundation.clearing',
        'foundation.dropdown': '../Vendor/foundation/js/foundation/foundation.dropdown',
        'foundation.equalizer': '../Vendor/foundation/js/foundation/foundation.equalizer',
        'foundation.interchange': '../Vendor/foundation/js/foundation/foundation.interchange',
        'foundation.joyride': '../Vendor/foundation/js/foundation/foundation.joyride',
        'foundation.magellan': '../Vendor/foundation/js/foundation/foundation.magellan',
        'foundation.offcanvas': '../Vendor/foundation/js/foundation/foundation.offcanvas',
        'foundation.orbit': '../Vendor/foundation/js/foundation/foundation.orbit',
        'foundation.reveal': '../Vendor/foundation/js/foundation/foundation.reveal',
        'foundation.tab': '../Vendor/foundation/js/foundation/foundation.tab',
        'foundation.tooltip': '../Vendor/foundation/js/foundation/foundation.tooltip',
        'foundation.topbar': '../Vendor/foundation/js/foundation/foundation.topbar'
    },
    shim: {
        /* Foundation */
        'foundation.core': {
            deps: [
                'jquery',
                'modernizr'
            ],
            exports: 'Foundation'
        },
        'foundation.abide': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.accordion': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.alert': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.clearing': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.dropdown': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.equalizer': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.interchange': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.joyride': {
            deps: [
                'foundation.core',
                'foundation.cookie'
            ]
        },
        'foundation.magellan': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.offcanvas': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.orbit': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.reveal': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.tab': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.tooltip': {
            deps: [
                'foundation.core'
            ]
        },
        'foundation.topbar': {
            deps: [
                'foundation.core'
            ]
        },

        /* Vendor Scripts */
        'fastclick': {
            exports: 'FastClick'
        },
        'jquery.cookie': {
            deps: [
                'jquery'
            ]
        },
        'jquery.drilldown': {
            deps: [
                'jquery'
            ]
        },
        'jquery.multiselect': {
            deps: [
                'jquery'
            ]
        },
        'jquery.visible': {
            deps: [
                'jquery'
            ]
        },
        'jquery.maskinput': {
            deps: [
                'jquery'
            ]
        },
        'jquery.validate': {
            deps: [
                'jquery'
            ]
        },
        'lightgallery': {
            deps: [
                'jquery'
            ]
        },
        'modernizr': {
            exports: 'Modernizr'
        },
        'placeholder': {
            exports: 'Placeholder'
        }
    },
    'deps': [
        'modernizr',
        'main'
    ]
};
