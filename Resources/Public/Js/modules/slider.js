define([
    'jquery',
    'slick'
], function($) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {

            var slidesToShow = 1,
                $sliderObjects = $('.js-slider').not('.slick-initialized'),
                $gallerySliderObjects = $('.js-gallery-slider').not('.slick-initialized'),
                $sliderArrowPrev,
                $sliderArrowPrevGallery,
                $sliderArrowNext,
                $sliderArrowNextGallery,
                $headsliderArrowPrev,
                $headsliderArrowNext,
                $sliderPlay,
                $sliderBreak,
                $sliderObject;

            //$headsliderArrowPrev = '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--prev o-slider__arrow o-slider__arrow--prev slick-arrow"><span class="h-sr-only">Zurück</span><img src="/typo3conf/ext/drk_template2016/Resources/Public/Images/png/arrow-left.png" width="42" height="80"></button>';

            //$headsliderArrowNext = '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--next o-slider__arrow o-slider__arrow--next slick-arrow"><span class="h-sr-only">Weiter</span><img src="/typo3conf/ext/drk_template2016/Resources/Public/Images/png/arrow-right.png" width="42" height="80"></button>';

            //$sliderPlay = '<button type="button" class="c-news-slider__play h-hide" title="Abspielen des Sliders"><span class="h-sr-only" aria-label="Symbolgrafik eines Play-Button">Abspielen</span><img src="/typo3conf/ext/drk_template2016/Resources/Public/Images/svg/play.svg" title="Play-Button" width="10" height="20"></button>';

            //$sliderBreak = '<button type="button" class="c-news-slider__break" title="Pausieren des Sliders"><span class="h-sr-only" aria-label="Symbolgrafik eines Pause-Button">Pause</span><img src="/typo3conf/ext/drk_template2016/Resources/Public/Images/svg/pause.svg" title="Pause-Button" width="10" height="20"></button>';

            $headsliderArrowPrev =
              '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--prev ' +
              'o-slider__arrow o-slider__arrow--prev slick-arrow">' +
              '<span class="h-sr-only">Zurück</span>' +
              '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
              '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
              '/Images/svg/spritemap.svg#sprite-arrow-left-large">' +
              '</use></svg></button>';

            $headsliderArrowNext =
              '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--next ' +
              'o-slider__arrow o-slider__arrow--next slick-arrow">' +
              '<span class="h-sr-only">Weiter</span>' +
              '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
              '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
              '/Images/svg/spritemap.svg#sprite-arrow-right-large">' +
              '</use></svg></button>';


            $sliderPlay =
              '<button type="button" class="c-news-slider__play h-hide"' +
              'title="Abspielen des Sliders">' +
              '<span class="h-sr-only" aria-label="Symbolgrafik eines Play-Button">Weiter</span>' +
              '<svg role="img" class="o-slider__play-icon" aria-label="Play-Button">' +
              '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
              '/Images/svg/spritemap.svg#sprite-play">' +
              '</use></svg></button>';

            $sliderBreak =
              '<button type="button" class="c-news-slider__break"' +
              'title="Pausieren des Sliders">' +
              '<span class="h-sr-only" aria-label="Symbolgrafik eines Pause-Button">Weiter</span>' +
              '<svg role="img" class="o-slider__play-icon" aria-label="Play-Button">' +
              '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
              '/Images/svg/spritemap.svg#sprite-pause">' +
              '</use></svg></button>';


            $sliderArrowPrev =
                '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--prev ' +
                'o-slider__arrow o-slider__arrow--prev slick-arrow">' +
                '<span class="h-sr-only">Zurück</span>' +
                '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
                '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
                '/Images/svg/spritemap.svg#sprite-arrow-left-large">' +
                '</use></svg></button>';

            $sliderArrowNext =
                '<button type="button" class="c-news-slider__arrow c-news-slider__arrow--next ' +
                'o-slider__arrow o-slider__arrow--next slick-arrow">' +
                '<span class="h-sr-only">Weiter</span>' +
                '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
                '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
                '/Images/svg/spritemap.svg#sprite-arrow-right-large">' +
                '</use></svg></button>';

            $sliderArrowPrevGallery =
                '<button type="button" class="c-gallery-slider__arrow c-gallery-slider__arrow--prev ' +
                'o-slider__arrow o-slider__arrow--prev slick-arrow">' +
                '<span class="h-sr-only">Zurück</span>' +
                '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
                '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
                '/Images/svg/spritemap.svg#sprite-arrow-left-large">' +
                '</use></svg></button>';

            $sliderArrowNextGallery =
                '<button type="button" class="c-gallery-slider__arrow c-gallery-slider__arrow--next ' +
                'o-slider__arrow o-slider__arrow--next slick-arrow">' +
                '<span class="h-sr-only">Weiter</span>' +
                '<svg role="img" class="o-slider__arrow-icon" aria-label="Symbolgrafik eines Pfeiles">' +
                '<use xlink:href="/typo3conf/ext/drk_template2016/Resources/Public' +
                '/Images/svg/spritemap.svg#sprite-arrow-right-large">' +
                '</use></svg></button>';


            $sliderObjects.each(function() {
                slidesToShow = $(this).data('slides-to-show');

                $sliderObject = $(this).hasClass('c-header-slider') ?

                  $(this).slick({
                    autoplay: true,
                    autoplaySpeed: 4000,
                    accessibility: true,
                    adaptiveHeight: false,
                    dots: false,
                    dotsClass: 'o-slider__nav',
                    infinite: true,
                    slidesToScroll: slidesToShow,
                    slidesToShow: slidesToShow,
                    prevArrow: $headsliderArrowPrev,
                    nextArrow: $headsliderArrowNext,
                    pauseOnHover: false,
                    useCSS: true,
                    easing: 'swing',
                    speed: 500,
                    width: '100%',
                    responsive: [
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                    }) :

                    $(this).slick({
                      autoplay: true,
                      autoplaySpeed: 4000,
                      accessibility: true,
                      adaptiveHeight: false,
                      dots: false,
                      dotsClass: 'o-slider__nav',
                      slide: '.o-slider__item, .js-slider-item, .o-section, .o-cms-content',
                      infinite: true,
                      slidesToScroll: slidesToShow,
                      slidesToShow: slidesToShow,
                      prevArrow: $sliderArrowPrev,
                      nextArrow: $sliderArrowNext,
                      pauseOnHover: true,
                      useCSS: true,
                      easing: 'swing',
                      speed: 500,
                      width: '100%',
                      responsive: [
                          {
                              breakpoint: 720,
                              settings: {
                                  slidesToShow: 1,
                                  slidesToScroll: 1
                              }
                          }
                      ]
                  });


              $sliderObject.find('.o-slider__item, .js-slider-item, .o-section, .o-cms-content').length > 1 &&
              $sliderObject.find('.o-slider__item, .js-slider-item, .o-section, .o-cms-content').length > slidesToShow &&
              ( $sliderObject.prepend($sliderBreak).prepend($sliderPlay),
                  $sliderObject.find('.c-news-slider__break').on('click', {sO:$sliderObject}, function (event)
                  {
                    $(this).hasClass('h-hide') || ($(this).addClass('h-hide'),
                      event.data.sO.find('.c-news-slider__play').removeClass('h-hide').focus()),
                      event.data.sO.slick('slickPause');
                  }),
                  $sliderObject.find('.c-news-slider__play').on('click', {sO:$sliderObject}, function (event)
                  {
                    $(this).hasClass('h-hide') || ($(this).addClass('h-hide'),
                      event.data.sO.find('.c-news-slider__break').removeClass('h-hide').focus()),
                      event.data.sO.slick('slickPlay');
                  }),
                  $sliderObject.slick('slickPlay')
              );
            });

            $gallerySliderObjects.each(function() {
                slidesToShow = $(this).data('slides-to-show');

                $sliderObject = $(this).slick({
                    autoplay: false,
                    autoplaySpeed: 4000,
                    accessibility: true,
                    adaptiveHeight: false,
                    dots: false,
                    dotsClass: 'o-slider__nav',
                    slide: '.o-slider__item',
                    infinite: true,
                    slidesToScroll: slidesToShow,
                    slidesToShow: slidesToShow,
                    prevArrow: $sliderArrowPrevGallery,
                    nextArrow: $sliderArrowNextGallery,
                    pauseOnHover: true,
                    useCSS: true,
                    easing: 'swing',
                    speed: 500,
                    width: '100%',
                    responsive: [
                        {
                            breakpoint: 720,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1
                            }
                        }
                    ]
                });

              $sliderObject.find('.o-slider__item').length > 1 &&
              $sliderObject.find('.o-slider__item').length > slidesToShow &&
              ($sliderObject.prepend($sliderBreak).prepend($sliderPlay),
               $sliderObject.find('.c-news-slider__break').on('click', {sO:$sliderObject}, function (event)
              {
                $(this).hasClass('h-hide') || ($(this).addClass('h-hide'),
                  event.data.sO.find('.c-news-slider__play').removeClass('h-hide').focus()),
                  event.data.sO.slick('slickPause');
              }),
              $sliderObject.find('.c-news-slider__play').on('click', {sO:$sliderObject}, function (event)
              {
                $(this).hasClass('h-hide') || ($(this).addClass('h-hide'),
                  event.data.sO.find('.c-news-slider__break').removeClass('h-hide').focus()),
                  event.data.sO.slick('slickPlay');
              }),
              $sliderObject.slick('slickPlay'));
            });
        }
    };

    return obj;
});
