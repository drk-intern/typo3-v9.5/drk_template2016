define([
    'jquery'
], function($) {
    'use strict';

    var obj = {
        $form: null,
        $zipField: null,
        $mailField: null,
        $urlField: null,
        $chapterSelectField: null,
        $chapterSelectContainer: null,
        $submitButton: null,
        $informationField: null,
        // Standard-Konfiguration
        _timeout: 500,
        // Klasse, die gesetzt wird, wenn Ajax-Anfragen im Hintergrund ausgeführt werden
        loadingClass: 'processing',
        // Klasse, die im Fehlerfall (invalide PLZ oder kein Kreisverband gefunden) gesetzt wird
        errorClass: 'error',

        /**
         * start referencing fields if correct form class is found
         */
        init: function() {
            // jQuery-Referenz auf das Formular-Element
            this.$form = $('.o-form--membership__application');
            if (this.$form.length) {
                // jQuery-Referenz auf das Eingabefeld für die Postleitzahl
                this.$zipField = $('#powermail_field_144');
                // jQuery-Referenz auf das Eingabefeld für den Kreisverband
                this.$chapterSelectField =
                    this.$form.find('#powermail_field_145');
                this.$chapterSelectContainer =
                    this.$form.find('#powermail_fieldwrap_145').addClass('hidden');
                this.$urlField =
                    this.$form.find('#powermail_field_organisation_url');
                this.$mailField =
                    this.$form.find('#powermail_field_organisation_mail');
                this.$informationField =
                    this.$form.find('#powermail_fieldwrap_148').addClass('hidden');

                this.$submitButton =
                    this.$form.find('.powermail_submit');

                // Objekt nur initiieren, wenn auch eine URL ausgelesen werden konnte
                this._lockForm();
                this.$zipField.on('keyup change input',
                    $.proxy(this._handleZipInput, this));

                this.$form.on('submit', $.proxy(this._submit, this));
            }

        },

        /**
         * Ziel-URL für die Ajax-Anfragen
         *
         * @returns {string}
         */
        getRequestUrl: function() {
            return this.$zipField.attr('data-zipsuggesturl');
        },

        /**
         * Funktion handhabt Eingaben im PLZ-Feld und wartet bis die Eingabe
         * vollständig ist, bis eine Anfrage ausgelöst wird
         *
         * @private
         */
        _handleZipInput: function() {
            clearTimeout(this._timeoutId);
            this._timeoutId = setTimeout(
                $.proxy(this._requestLocalChapters, this),
                this._timeout
            );
        },

        //
        /**
         * Funktion fragt bei gegebener PLZ die dazugehörigen Kreisverbände beim Server an
         *
         * @private
         */
        _requestLocalChapters: function() {
            var zipcode = this.$zipField.val();
            if (zipcode !== this._oldZipcode) {
                this._oldZipcode = zipcode;
                this.$zipField.addClass(this.loadingClass);

                $.ajax({
                    method: 'get',
                    dataType: 'json',
                    url: this.getRequestUrl() + zipcode,
                    success: $.proxy(this._handleResponse, this)
                });
            }
        },

        /**
         * Funktion wertet die Server-Antwort und die Kreisverbandsdaten aus und handelt entsprechend
         *
         * @param {json} response
         * @private
         */
        _handleResponse: function(response) {
            var chapters, orgId;
            this.$zipField.removeClass(this.loadingClass);
            if (response.orgdata) {

                chapters = [];
                for (orgId in response.orgdata) {
                    if (response.orgdata.hasOwnProperty(orgId)) {
                        chapters.push(response.orgdata[orgId]);
                    }
                }

                if (chapters.length > 0) {
                    this._renderChapterSelection(chapters);
                }

                this._handleSuccess();
            } else {
                this._handleError(response.warning);
                this._resetChapterInformation();
                this._hideChapterSelection();
            }

            // Formular kann in jedem Fall jetzt abgesendet werden
            this._unlockForm();
        },

        /**
         *
         * Funktion erzeugt ein Auswahlfeld, falls mehrere mögliche
         * Kreisverbände zur Auswahl stehen
         *
         * @param {array} chapters
         * @private
         */
        _renderChapterSelection: function(chapters) {
            var options = '', i, len = chapters.length;
            for (i = 0; i < len; i += 1) {
                options +=
                    '<option data-mail="' + chapters[i].data.mail + '" ' +
                    'data-url="' + chapters[i].data.url + '" value="' +
                    i +
                    '">' +
                    chapters[i].name +
                    '</option>';
            }

            this.$chapterSelectField.append(options);
            this.$chapterSelectContainer.removeClass('hidden');
            this.$chapterSelectField.on('change',
                $.proxy(this._handleSelectChange, this));
        },

        /**
         *
         * Funktion nimmt Änderungen am Formular vor, falls ein Fehler vorliegt
         *
         * @param {string} message
         * @private
         */
        _handleError: function(message) {
            this.$error = $('<p class="error"></p>');
            this.$chapterSelectField.parent().before(this.$error.html(message));
            this.$zipField.addClass(this.errorClass);
            this.$chapterSelectContainer.addClass('hidden');
            this.$informationField.addClass('hidden');
        },

        /**
         *
         * Funktion macht Änderungen der Funktion _handleError rückgängig
         *
         * @private
         */
        _handleSuccess: function() {
            if (this.$error) {
                this.$zipField.removeClass(this.errorClass);
                this.$error.detach();
            }
        },

        /**
         *
         * Funktion fängt Änderungen an der Kreisverbandsauswahl ab und
         * lässt die Kreisverbandsdaten aktualisieren
         *
         * @param {Object} e
         * @private
         */
        _handleSelectChange: function(e) {
            this._updateChapterInformation($(e.target).find('option:selected'));
        },

        /**
         *
         * Funktion entfernt die Auswahl für Kreisverbände
         *
         * @private
         */
        _hideChapterSelection: function() {
            if (this.$chapterSelectField) {
                this.$chapterSelectContainer.addClass('hidden');
                this.$informationField.addClass('hidden');
            }
        },

        /**
         *
         * Funktion aktualisiert auf Basis der Kreisverbandsdaten die
         * definierten Felder innerhalb des Formulars
         *
         * @param {Object} selectedOption
         * @private
         */
        _updateChapterInformation: function(selectedOption) {
            if (selectedOption !== undefined && selectedOption.val() !== '-1') {
                this.$mailField.val(selectedOption.data('mail'));
                this.$urlField.val(selectedOption.data('url'));
                this.$informationField.removeClass('hidden');
            } else {
                this._resetChapterInformation();
            }
        },

        /**
         *
         * Funktion leert vorher ausgefüllte Felder für Kreisverbandsdaten
         * im Fehlerfall
         *
         * @private
         */
        _resetChapterInformation: function() {
            this.$mailField.val('');
            this.$urlField.val('');
            this.$informationField.addClass('hidden');
        },

        /**
         *
         * Funktion sperrt das Formular und den Senden-Button für Submit-Events
         *
         * @private
         */
        _lockForm: function() {
            this._locked = true;
            this.$submitButton.prop('disabled', true);
        },

        // Funktion entsperrt das Formular und den Senden-Button für Submit-Events
        _unlockForm: function() {
            this._locked = false;
            this.$submitButton.prop('disabled', false);
        },

        /**
         *
         * Funktion prüft beim Absenden des Formulars, ob es gesperrt ist
         * und unterdrückt ggf. das Senden der Daten
         *
         * @param {Object} e
         * @private
         */
        _submit: function(e) {
            if (this._locked) {
                e.preventDefault();
                e.stopPropagation();
            }
        }
    };

    return obj;
});
