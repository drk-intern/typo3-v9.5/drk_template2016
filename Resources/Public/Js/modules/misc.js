/**
 *
 */
define([
    'jquery',
    'jquery.maskinput',
    'jquery.validate',
    'fastclick',
    'jquery.drilldown',
    'jquery.visible'
], function($) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {

            var drilldownOptions = {
                selector: '.c-menu-drilldown__link',
                speed: 250,
                cssClass: {
                    container: 'c-menu-drilldown__inner',
                    root: 'c-menu-drilldown__root',
                    sub: 'c-menu-drilldown__sub',
                    back: 'c-menu-drilldown__back'
                }
            };

            $('.js-drilldown').drilldown(drilldownOptions);

            $('.js-check-has-value').each(function() {
                var fieldValue = $(this).val();

                if (fieldValue === '') {
                    $(this).removeClass('has-value');
                } else {
                    $(this).addClass('has-value');
                }
            });

            $('.js-check-has-value').focusout(function() {
                var fieldValue = $(this).val();

                if (fieldValue === '') {
                    $(this).removeClass('has-value');
                } else {
                    $(this).addClass('has-value');
                }
            });

            $('.js-field-editable').click(function() {
                var valueField = $(this).data('value-field'),
                    formField = $(this);

                $(valueField).focus(function() {
                    $(this).focusout(function() {
                        formField.val($(this).val());
                    });
                }).focus();
            });

            $(window).on(
                'DOMContentLoaded load resize scroll',
                function() {
                    $('.js-flyout').each(function() {
                        var $this = $(this);

                        if ($this.visible(true)) {
                            $this.
                                find('.js-toggle-visible').
                                addClass('is-in-viewport');
                        }

                        if (!$this.visible(true)) {
                            $this.
                                find('.js-toggle-visible').
                                removeClass('is-in-viewport');
                        }
                    });

                }
            );
        }
    };

    return obj;
});
