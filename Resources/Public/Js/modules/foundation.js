/**
 *
 */
define([
    'jquery',
    'foundation.abide',
    'foundation.accordion',
    'foundation.reveal',
    'foundation.tab',
    'foundation.topbar',
    'foundation.interchange',
    'jquery.multiselect'
], function($) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {
            $(document).foundation({
                abide: {
                    'validate_on_blur': true,
                    'error_labels': true,
                    'error_class': 'has-error',
                    patterns: {
                        alpha: /^[a-zA-Z]+$/,
                        zipcodes: /^(?!01000|99999)(0[1-9]\d{3}|[1-9]\d{4})$/
                    }
                },
                accordion: {
                    'active_class': 'is-active',
                    'content_class': 'js-accordion-content',
                    'multi_expand': true
                },
                reveal: {
                    'dismiss_modal_class': 'js-modal-close',
                    'bg_class': 'o-modal__bg',
                    'bg': $('.o-modal__bg')
                },
                tab: {
                    'active_class': 'is-active'
                },
                topbar: {
                    'sticky_class': 'js-sticky'
                }
            });
            // get the placeholder value from data-prompt inside
            // ke-search partial for select filter so we can use
            // translation
            $('.ke_search__category-filter').each(function() {
                var $filterElement = $(this);
                $filterElement.multiselect({
                    columns: 1,
                    placeholder: $filterElement.data('prompt')
                });
            });
        }
    };

    return obj;
});
