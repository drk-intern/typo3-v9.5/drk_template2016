/**
 *
 */
define([
    'jquery'
], function($) {
    'use strict';

    var obj = {
        srcNodeRef: null,

        /**
         * Initialize
         */
        init: function() {
            this.srcNodeRef = $('.tx-drk-jobboard');
            if (this.srcNodeRef.length > 0) {
                this.srcNodeRef = this.srcNodeRef[0];
                this.render();
            }
        },
        render: function() {
            this._initSearchForm();
            this._initPreviewButton();
            this._initDeleteButton();
            var textarea = $(this.srcNodeRef).find('#vacancy');
            if (textarea.length > 0) {
                this._initTinyMce();
            }
        },
        _initSearchForm: function() {
            var toggle = $('.c-jobboard-form-search__section-toggle'),
                tabAdvanced = $('.c-jobboard-form-search__section-advanced');

            toggle.click(function(e) {
                e.preventDefault();
                if ($(this).is(':hidden')) {
                    toggle.text('Weniger Optionen');
                } else {
                    toggle.text('Mehr Optionen');
                }
                tabAdvanced.slideToggle();
            });
        },
        _initPreviewButton: function() {
            $(this.srcNodeRef).find('#tx_jobboard_previewbutton').on('click', function(e) {
                e.preventDefault();
                var form = $(this).closest('form'),
                    originalAction = form.attr('action');

                form.attr('action', $(this).attr('formaction'));
                form.attr('target', '_blank');

                form.submit();

                form.attr('action', originalAction);
                form.attr('target', '');

                return false;
            });
        },
        _initDeleteButton: function() {
            $(this.srcNodeRef).find('a.jobDelete').bind('click', function(event) {
                return window.confirm('Wollen Sie die Stellenausschreibung wirklich löschen?');
            });
        },
        _initTinyMce: function() {
            /* global require */
            /* global tinymce */
            require(['tinymce'], function() {
                // jshint camelcase: false
                // jscs:disable requireCamelCaseOrUpperCaseIdentifiers
                tinymce.init({
                    plugins: 'fullscreen, link',
                    selector: '#vacancy',
                    browser_spellcheck: true,
                    // das slash bei language url ist häßlich, aber da im live system base nicht gesetzt ist..
                    language_url: '/typo3conf/ext/drk_jobboard/Resources/Public/JavaScript/TinyMce/Langs/de.js',
                    element_format: 'html',
                    fix_list_elements: true,
                    forced_root_block: 'p',
                    toolbar: 'undo redo | cut copy paste | bold bullist numlist outdent indent blockquote | link | ' +
                    'fullscreen | removeformat',
                    menubar: false,
                    statusbar: false
                });
            });
        }
    };

    return obj;
});
