/********************\
 Show scroll to top button
 \********************/

define([
    'jquery'
], function($) {
    'use strict';

    var obj = {
        btn: '.c-toplink',

        init: function() {
            obj.btn = $(obj.btn);
            $(window).scroll(function() {
                if ($(this).scrollTop() > 100) {
                    obj.btn.fadeIn();
                } else {
                    obj.btn.fadeOut();
                }
            });

            obj.btn.click(function() {
                $('html, body').animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
        }
    };

    return obj;
});
