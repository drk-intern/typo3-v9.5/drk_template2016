/**
 *
 */
define([
    'jquery',
    'moment',
    'jquery.pikaday'
], function($, moment) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {
            // use default en language settings
            var languageOverrides = {},
                locale = window.navigator.userLanguage ||
                window.navigator.language,
                // start from year current - 100 years back
                yearRangeStart = new Date().getFullYear() - 100,
                // maximum is one year into the future
                yearRangeEnd = new Date().getFullYear() + 1,
                datepickerConfig = {
                firstDay: 1,
                yearRange: [yearRangeStart, yearRangeEnd]
            };

            if (locale === 'de') {
                moment.locale('de');
                languageOverrides = {
                    format: 'DD.MM.YYYY',
                    i18n: {
                        previousMonth: 'Zurück',
                        nextMonth: 'Weiter',
                        months: ['Januar',
                            'Februar',
                            'März',
                            'April',
                            'Mai',
                            'Juni',
                            'Juli',
                            'August',
                            'September',
                            'Oktober',
                            'November',
                            'Dezember'],
                        weekdays: ['Sonntag',
                            'Montag',
                            'Dienstag',
                            'Mittwoch',
                            'Donnerstag',
                            'Freitag',
                            'Samstag'],
                        weekdaysShort: ['So',
                            'Mo',
                            'Di',
                            'Mi',
                            'Do',
                            'Fr',
                            'Sa']
                    }
                };
            }

            $.extend(datepickerConfig, languageOverrides);
            $('.js-datepicker').pikaday(datepickerConfig);
        }
    };

    return obj;
});
