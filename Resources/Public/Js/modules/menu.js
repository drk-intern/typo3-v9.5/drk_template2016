/**
 *
 */
define([
    'jquery'
], function($) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {

            var menuIsHidden = true;

            $('.js-menu-main-toggle').on(
                'click',
                function(ev) {
                    var $this = $(this),
                        $icon = $this.find('.js-toggle'),
                        $label = $this.find('.js-toggle-label'),
                        $toggleTarget = $($this.data('toggle-target'));

                    ev.preventDefault();

                    $toggleTarget.toggleClass('is-active');
                    menuIsHidden = !menuIsHidden;

                    if (menuIsHidden) {
                        $label.text('Menü');
                        $icon.toggleClass('is-active');
                    } else {
                        $label.text('Schließen');
                        $icon.toggleClass('is-active');
                        $('.is-highlighted ul').show();
                    }
                }
            );

            $('.js-menu-meta-toggle').on(
                'click',
                function(ev) {
                    var $toggleTarget =
                        $($(this).data('toggle-target'));

                    ev.preventDefault();

                    $toggleTarget.toggleClass('is-active').parent().toggleClass('is-active');
                }
            );
        },

        dropdownMenuBehaviour: function() {
            $('.c-menu-main__list--l2').hide();
            $('.c-menu-main--sublist__call > a').on('click', function(event) {
                event.preventDefault();
            });

            $('#c-menu-main').find('a').on('click', function() {
                var link,
                    closestUl,
                    count,
                    parallelActiveLinks,
                    closestLi,
                    icon,
                    linkStatus;

                link = $(this);
                closestUl = link.closest('ul');
                parallelActiveLinks = closestUl.find('.is-active');
                closestLi = link.closest('li');
                icon = link.parent().find('.collapseicon');
                linkStatus = closestLi.hasClass('is-active');
                count = 0;

                closestUl.find('ul').slideUp(function() {
                    if (++count === closestUl.find('ul').length) {
                        parallelActiveLinks.removeClass('is-active');
                        icon.text('+');
                    }
                });

                if (!linkStatus) {
                    closestLi.children('ul').slideDown('fast');
                    closestLi.addClass('is-active');
                    icon.text('-');
                }
            });
        }
    };

    return obj;
});
