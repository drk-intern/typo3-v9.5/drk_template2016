/**
 *
 */
define([
    'jquery',
    'lightgallery'
], function($) {
    'use strict';

    var obj = {

        /**
         * Initialize
         */
        init: function() {
            $('.js-lightbox-gallery').lightGallery({
                selector: 'figure a'
            });
            $('.c-mediathek').lightGallery({
                selector: '.o-media__link'
            });
            $('.c-addresses__imglink').lightGallery({
                selector: 'a'
            });
        }
    };

    return obj;
});
