/**
 *
 */
define([
        'jquery',
        'modules/foundation',
        'modules/lightgallery',
        'modules/menu',
        'modules/slider',
        'modules/misc',
        'modules/top-link',
        'svg4everybody'
    ],
    function($,
        Foundation,
        lightgallery,
        Menu,
        Slider,
        Misc,
        TopLink,
        svg4everybody) {
        'use strict';

        Foundation.init();
        lightgallery.init();
        Menu.init();
        Menu.dropdownMenuBehaviour();
        Slider.init();
        Misc.init();
        TopLink.init();
        svg4everybody();
    });
