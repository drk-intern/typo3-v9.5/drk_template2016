// https://github.com/tomusdrw/grunt-sync
module.exports = {
    src: {
        cwd: './src',
        src: '<%= files.src %>',
        dest: '<%= dirs.dest.dest %>',
        expand: true,
        updateAndDelete: false
    },
    additionalConfiguration: {
        cwd: './',
        src: 'data/private/AdditionalConfiguration.php',
        dest: 'typo3conf/AdditionalConfiguration.php',
        updateAndDelete: false
    },
    localConfiguration: {
        cwd: './',
        src: 'data/config/LocalConfigurationDev.php',
        dest: 'typo3conf/LocalConfiguration.php',
        updateAndDelete: false
    }
};
