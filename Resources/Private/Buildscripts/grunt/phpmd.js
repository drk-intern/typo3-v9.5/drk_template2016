// https://github.com/alappe/grunt-phpmd
module.exports = {
    options: {
        bin: 'bin/phpmd',
        reportFormat: 'text',
        rulesets: 'codesize,design,unusedcode'
    },
    src: {
        dir: 'src'
    }
};
