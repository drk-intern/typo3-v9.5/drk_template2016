// https://github.com/dylang/grunt-notify
module.exports = {
    options: {
        enabled: true,
        'max_jshint_notifications': 5
    }
};
