// https://github.com/firstandthird/load-grunt-config
module.exports = {
    default: [
        'watch'
    ],
    build: [
        'clean:all',
        'modernizr',
        'js',
        'images',
        //'copy:src',
        //'test:backend',
        //'copy:vendor',
        //'copy:additionalConfiguration',
        //'copy:htaccess'
    ],
    init: [
        'build',
        'watch'
    ],
    test: [
        'test:frontend',
        'test:backend'
    ],
    'test:frontend': [
        'jshint',
        'jscs',
        'scsslint'
    ],
    'test:backend': [
        'phplint',
        'phpcs',
        'phpmd',
        'phpunit'
    ],
    fix: [
        'phpcbf'
    ],
    images: [
        //'favicons',
        'svg-spritemap',
        'svg-icons',
        'copy:png',
        'imagemin',
        'scss'
    ],
    'svg-spritemap': [
        'grunticon-spritemapFallback',
        'svgstore',
        'copy:fallbackpng'
    ],
    'svg-icons': [
        'svgmin:icons',
        'grunticon:backgroundIcons'
    ],
    'grunticon-spritemapFallback': [
        'svgmin:sprites',
        'grunticon:spritemapFallback'
    ],
    'grunticon-sass': [
        'svgmin:icons',
        'grunticon:backgroundIcons'
    ],
    js: [
        //'jshint',
        //'jscs',
        'requirejs'
    ],
    scss: [
        'clean:css',
        'scsslint',
        'sass',
        'postcss',
    ],
    'scss-easy': [
        'clean:css',
        'scsslint',
        'sass'
    ]
};
