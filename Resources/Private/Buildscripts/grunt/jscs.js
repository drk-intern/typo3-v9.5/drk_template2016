// https://github.com/jscs-dev/grunt-jscs
module.exports = {
    js: {
        fix: true,
        options: {
            config: '.jscsrc'
        },
        files: {
            src: '<%= files.js %>'
        }
    }
};
