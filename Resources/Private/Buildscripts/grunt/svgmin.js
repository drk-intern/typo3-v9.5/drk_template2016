// https://github.com/sindresorhus/grunt-svgmin
module.exports = {
    options: {
        plugins: [
            {
                removeViewBox: false
            },
            {
                removeUselessStrokeAndFill: false
            }
        ]
    },
    icons: {
        cwd: '<%= dirs.src.images %>/svg/background-icons',
        dest: '<%= dirs.temp %>/images/svg/background-icons',
        expand: true,
        ext: '.svg',
        src: ['**/*.svg']
    },
    sprites: {
        cwd: '<%= dirs.src.images %>/svg/spritemap',
        dest: '<%= dirs.temp %>/images/svg/spritemap/optimized',
        expand: true,
        ext: '.svg',
        src: ['**/*.svg']
    }
};
