// https://github.com/doctyper/grunt-modernizr
module.exports = {
    build: {
        crawl: false,
        dest: '<%= dirs.dest.js %>/modernizr.custom.js',
        devFile: false,
        options: [
            'domPrefixes',
            'prefixes',
            'addTest',
            'hasEvent',
            'mq',
            'prefixed',
            'prefixedCSS',
            'prefixedCSSValue',
            'testAllProps',
            'testProp',
            'testStyles',
            'setClasses'
        ],
        tests: [
            'forms_placeholder',
            'history',
            'inlinesvg',
            'svg',
            'touch',
            'csstransforms',
            'csstransforms3d',
            'cssvwunit',
            'touchevents',
            'video'
        ],
        uglify: true
    }
};
