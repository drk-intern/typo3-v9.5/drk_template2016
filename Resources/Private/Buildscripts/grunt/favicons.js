// https://github.com/gleero/grunt-favicons
module.exports = {
    options: {
        appleTouchBackgroundColor: '#ffffff',
        appleTouchPadding: 0,
        html: '<%= dirs.partials %>/generated/generated-favicons.html',
        HTMLPrefix: '<%= dirs.dest.images %>/png/',
        sharp: 1,
        tileColor: '#e60005'
    },
    icons: {
        src: '<%= dirs.src.images %>/png/favicon.png',
        dest: '<%= dirs.dest.images %>/png'
    }
};
