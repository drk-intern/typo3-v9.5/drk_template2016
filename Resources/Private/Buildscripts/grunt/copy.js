// https://github.com/gruntjs/grunt-contrib-copy
module.exports = {
    src: {
        cwd: '<%= dirs.src.src %>',
        src: '<%= files.src %>',
        dest: '<%= dirs.dest.dest %>/',
        expand: true,
        dot: true
    },
    vendor: {
        cwd: './vendor',
        src: '<%= files.vendor %>',
        dest: '<%= dirs.dest.dest %>/',
        expand: true,
        dot: true
    },
    release: {
        cwd: '<%= dirs.dest.config %>',
        src: '<%= files.release %>',
        dest: '<%= dirs.dest.release %>/<%= pkg.version %>/',
        expand: true,
        dot: true
    },
    stagingAdditionalConfiguration: {
        cwd: './',
        src: '<%= dirs.data %>/private/<%= files.stagingAdditionalConfiguration %>',
        dest: '<%= dirs.dest.release %>/<%= pkg.version %>/AdditionalConfiguration.php',
        flatten: true
    },
    liveAdditionalConfiguration: {
        cwd: './',
        src: '<%= dirs.data %>/private/<%= files.liveAdditionalConfiguration %>',
        dest: '<%= dirs.dest.release %>/<%= pkg.version %>/AdditionalConfiguration.php',
        flatten: true
    },
    additionalConfiguration: {
        cwd: '<%= dirs.data %>/private',
        dest: '<%= dirs.dest.config %>/',
        src: '<%= files.additionalConfiguration %>',
        expand: true,
        flatten: true
    },
    stagingHtaccess: {
        cwd: './',
        src: '<%= dirs.data %>/private/_.StagingHtaccess',
        dest: '<%= dirs.dest.release %>/<%= pkg.version %>/_.htaccess',
        flatten: true,
    },
    liveHtaccess: {
        cwd: './',
        src: '<%= dirs.data %>/private/_.LiveHtaccess',
        dest: '<%= dirs.dest.release %>/<%= pkg.version %>/_.htaccess',
        flatten: true,
        rename: function(dest, src) {
            'use strict';
            return dest + src.replace(/_.Live/g, '.h');
        }
    },
    htaccess: {
        cwd: './',
        src: '<%= dirs.data %>/private/_.htaccess',
        dest: '.htaccess',
        flatten: true,
        rename: function(dest, src) {
            'use strict';
            return dest + src.replace(/_/g, '');
        }
    },
    fallbackpng: {
        cwd: '<%= dirs.temp %>/images/svg',
        dest: '<%= dirs.dest.images %>/svg',
        expand: true,
        rename: function(dest, src) {
            'use strict';
            var path = require('path');

            // Prefix each fallback png with
            // spritemap.svg.svg- name into the destination
            // as needed for svg4everybody
            return path.join(
                dest,
                'spritemap.svg.svg-' + path.basename(src)
            );
        },
        src: ['*.png']
    },
    png: {
        cwd: '<%= dirs.src.images %>/png/',
        dest: '<%= dirs.temp %>/images/png/',
        expand: true,
        src: ['**/*.png']
    }
};

