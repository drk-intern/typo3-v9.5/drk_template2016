// https://github.com/SaschaGalley/grunt-phpcs
module.exports = {
    options: {
        bin: './vendor/bin/phpcs ' +
        '--ignore=src/frs_drk_search/Classes/Backend/Hooks/ModifyBackendData.php,src/frs_drk_donations/Vendor/*,',
        standard: 'PSR2'
    },
    src: {
        src: '<%= files.php %>'
    }
};
