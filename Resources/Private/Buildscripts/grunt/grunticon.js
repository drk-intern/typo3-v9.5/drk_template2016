// https://github.com/filamentgroup/grunticon
module.exports = {
    backgroundIcons: {
        files: [{
            cwd: './',
            expand: true,
            src: '<%= dirs.temp %>/images/svg/background-icons/**/*.svg',
            dest: '<%= dirs.src.scss %>/generated',
            ext: '.svg'
        }],
        options: {
            cssprefix: 'icon--',
            //corsEmbed: true,
            datapngcss: '_generated.grunticon.data-png.scss',
            datasvgcss: '_generated.grunticon.data-svg.scss',
            urlpngcss:  '_generated.grunticon.url-png.scss',
            //defaultHeight: '96px',
            //defaultWidth: '96px',
            //enhanceSVG: true,
            pngfolder: '../../../../../../temp/images/png/icon-fallbacks',
            pngpath: '../Images/png/icon-fallbacks',
            previewhtml: '../../../../../../temp/images/png/background-icons.html',
            template: 'grunt/config/grunticon.hbs'
        }
    },
    spritemapFallback: {
        files: [{
            expand: true,
            cwd: './',
            src: '<%= dirs.temp %>/images/svg/spritemap/optimized/*.svg',
            dest: '<%= dirs.temp %>/images/svg'
        }],
        options: {
            datasvgcss: '_generated.grunticon-svg.scss',
            cssprefix: 'svg-icon-',
            pngfolder: './'
        }
    }
};
