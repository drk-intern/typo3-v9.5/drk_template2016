// https://github.com/jgable/grunt-phplint
module.exports = {
    options: {
        swapPath: '/tmp'
    },
    src: '<%= files.php %>'
};
