// https://github.com/chuckmo/grunt-ssh
module.exports = {
    upload: {
        files: {
            './': '<%= dirs.dest.release %>/*.tar.gz'
        },
        options: {
            createDirectories: true,
            showProgress: true,
            srcBasePath: 'release'
        }
    }
};
