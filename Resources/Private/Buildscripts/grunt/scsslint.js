// https://github.com/ahmednuaman/grunt-scss-lint
module.exports = {
    allFiles: '<%= files.scss %>',
    options: {
        colorizeOutput: false,
        config: '.scss-lint.yml',
        exclude: '../Sass/7-trumps/_trumps.page-overrides.scss',
        maxBuffer: 300000000
    }
};
