// https://github.com/gruntjs/grunt-contrib-watch
module.exports = {
    // all: {
    //     files: '<%= files.watch %>',
    //     tasks: [
    //         'sync:src'
    //     ]
    // },
    js: {
        files: '<%= files.js %>',
        tasks: [
            'jscs:js',
            'sync:src'
        ]
    },
    php: {
        files: '<%= files.php %>',
        tasks: [
            // 'phpcs',
            'phplint'
        ]
    },
    sass: {
        files: [
            '<%= files.scss %>'
        ],
        tasks: [
            'sass',
            'sync:src'
        ]
    },
    templates: {
        files: [
            '<%= files.templates %>'
        ],
        tasks: [
            'sync:src'
        ]
    }
};
