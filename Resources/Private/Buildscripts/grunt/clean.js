// https://github.com/gruntjs/grunt-contrib-clean
module.exports = {
    all: {
        src: '<%= files.clean %>'
    },
    css: [
        '<%= dirs.dest.css %>/*'
    ],
    release: [
        '<%= dirs.dest.release %>/*'
    ]
};
