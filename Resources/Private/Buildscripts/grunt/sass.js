// https://github.com/sindresorhus/grunt-sass
module.exports = {
    options: {
        outputStyle: 'compressed',
        sourceMap: true
    },
    build: {
        files: '<%= files.sassBuild %>',
        expand: true
    }
};
