// https://github.com/gruntjs/grunt-contrib-imagemin
module.exports = {
    options: {
        optimizationLevel: 3,
        svgoPlugins: [{
            removeViewBox: false
        }]
    },
    fallback: {
        cwd: '<%= dirs.temp %>/images/background/fallback/',
        dest: '<%= dirs.dest.images %>/background/fallback',
        expand: true,
        src: ['**/*.{png,jpg,gif}']
    },
    jpg: {
        cwd: '<%= dirs.src.images %>/jpg/',
        dest: '<%= dirs.dest.images %>/jpg',
        expand: true,
        src: ['**/*.jpg']
    },
    png: {
        cwd: '<%= dirs.temp %>/images/png/',
        dest: '<%= dirs.dest.images %>/png',
        expand: true,
        src: '**/*.png'
    }
};

