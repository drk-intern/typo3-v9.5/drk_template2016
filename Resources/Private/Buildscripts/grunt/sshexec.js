// https://github.com/chuckmo/grunt-ssh
module.exports = {
    extractTarball: {
        command: [
            'cd <%= sshconfig[grunt.option(\'config\')].path %>',
            'tar -zxpf ./<%= pkg.version %>.tar.gz',
            'rm -rf ./<%= pkg.version %>.tar.gz'
        ].join(' && ')
    },
    createSymlinks: {
        command: [
            'ln -nfs <%= sshconfig[grunt.option(\'config\')].path %>/<%= pkg.version %>/ext ' +
            '<%= sshconfig[grunt.option(\'config\')].www %>/typo3conf/ '
        ].join(' && ')
    },
    moveAdditionalConfiguration: {
        command: [
            'cp <%= sshconfig[grunt.option(\'config\')].path %>/<%= pkg.version %>/AdditionalConfiguration.php ' +
            '<%= sshconfig[grunt.option(\'config\')].www %>/typo3conf/AdditionalConfiguration.php'
        ].join(' && ')
    },
    moveHtaccess: {
        command: [
            'cp <%= sshconfig[grunt.option(\'config\')].path %>/<%= pkg.version %>/_.htaccess ' +
            '<%= sshconfig[grunt.option(\'config\')].www %>/.htaccess'
        ].join(' && ')
    }
};
