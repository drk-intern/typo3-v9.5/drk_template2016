// https://github.com/nDmitry/grunt-postcss
module.exports = {
    options: {
        map: true,
        processors: [
            require('autoprefixer')({
                browsers: [
                    '> 1% in DE',
                    'Android >= 4.1',
                    'Explorer >= 8',
                    'Firefox >= 17',
                    'iOS >= 6',
                    'last 4 versions',
                    'Opera >= 12.1',
                    'Safari >= 5'
                ]
            })
        ]
    },
    all: {
        expand: true,
        flatten: false,
        src: '<%= files.css %>'
    }
};
