//https://github.com/mducharme/grunt-phpcbf
module.exports = {
    options: {
        bin: './vendor/bin/phpcbf ' +
        '--ignore=src/frs_drk_search/Classes/Backend/Hooks/ModifyBackendData.php,src/frs_drk_donations/Vendor/*,',
        standard: 'PSR2'
    },
    src: {
        src: '<%= files.php %>'
    }
};
