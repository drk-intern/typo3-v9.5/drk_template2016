// https://github.com/gruntjs/grunt-contrib-requirejs
module.exports = {
    frontend: {
        options: {
            almond: true,
            baseUrl: '<%= dirs.src.js %>/',
            insertRequire: ['main'],
            findNestedDependencies: true,
            mainConfigFile: '<%= dirs.src.js %>/config.js',
            name: '../Vendor/almond/almond',
            optimize: 'uglify2',
            out: '<%= dirs.dest.js %>/frontend.min.js',
            uglify2: {
                comments: '/copyright|license/i'
            },
            wrap: false
        }
    }
};
