// https://github.com/SaschaGalley/grunt-phpunit
module.exports = {
    options: {
        bin: './vendor/bin/phpunit',
        followOutput: true,
        failOnFailures: false,
        verbose: true
    },
    classes: {
        configuration: './phpunit.dist.xml'
    }
};
