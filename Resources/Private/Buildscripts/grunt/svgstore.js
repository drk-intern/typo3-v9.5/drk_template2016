// https://github.com/FWeinb/grunt-svgstore
module.exports = {
    options: {
        prefix: 'sprite-',
        svg: {
            version: '1.1',
            xmlns: 'http://www.w3.org/2000/svg',
            'xmlns:xlink': 'http://www.w3.org/1999/xlink'
        }
    },
    sprites: {
        files: {
            '<%= dirs.dest.images %>/svg/spritemap.svg': '<%= dirs.temp %>/images/svg/spritemap/optimized/*.svg'
        }
    }
};
