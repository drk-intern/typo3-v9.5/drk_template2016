// https://github.com/sindresorhus/grunt-shell
module.exports = {
    compressRelease: {
        command: [
            'cd <%= dirs.dest.release %>',
            'tar -zcf ./<%= pkg.version %>.tar.gz ./<%= pkg.version %>'
        ].join(' && ')
    },
    execOptions: {
        maxBuffer: Infinity
    },
    options: {
        stderr: false,
        stdout: true
    }
};
