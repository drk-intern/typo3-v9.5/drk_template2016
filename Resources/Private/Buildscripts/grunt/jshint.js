// https://github.com/gruntjs/grunt-contrib-jshint
module.exports = {
    options: {
        jshintrc: '.jshintrc'
    },
    src: '<%= files.js %>'
};
