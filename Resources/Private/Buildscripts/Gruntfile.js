module.exports = function(grunt) {
    'use strict';

    var cliTaskArgument,
        res;

    cliTaskArgument = grunt.cli.tasks[0];
    if (cliTaskArgument !== undefined && cliTaskArgument.indexOf('deploy') > -1) {
        res = cliTaskArgument.split(':');
        if (res.length > 1) {
            grunt.option('config', res[1]);
        } else {
            console.error('deploy:staging, deploy:live');
            return;
        }
    }

    // https://github.com/firstandthird/load-grunt-config
    require('load-grunt-config')(grunt, {
        // properties
        data: {
            files: grunt.file.readJSON('grunt/config/files.json'),
            dirs: grunt.file.readJSON('grunt/config/dirs.json'),
            pkg: grunt.file.readJSON('package.json')
        },
        loadGruntTasks: {
            pattern: ['grunt-*', '!grunt-template-jasmine-requirejs']
        },
        jitGrunt: {
            staticMappings: {
                'notify_hooks': 'grunt-notify',
                sftp: 'grunt-ssh',
                sshexec: 'grunt-ssh',
                scsslint: 'grunt-scss-lint',
                phpcbf: 'grunt-phpcbf',
                sync: 'grunt-sync',
                githooks: 'grunt-githooks'
            }
        }
    });

    // Desktop notifications
    //grunt.task.run('notify_hooks');
};
