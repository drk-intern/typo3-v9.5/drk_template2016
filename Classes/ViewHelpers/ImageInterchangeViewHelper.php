<?php

namespace Drk\DrkTemplate\ViewHelpers;

use InvalidArgumentException;
use RuntimeException;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Exception;
use TYPO3\CMS\Core\Resource\FileReference;
use UnexpectedValueException;

class ImageInterchangeViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * @var string
     */
    protected $tagName = 'img';

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @param ImageService $imageService
     */
    public function injectImageService(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * Initialize arguments.
     *
     * @return void
     * @throws Exception
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerTagAttribute(
            'alt',
            'string',
            'Specifies an alternate text for an image'
        );
        $this->registerTagAttribute(
            'ismap',
            'string',
            'Specifies an image as a server-side image-map. Rarely used. Look at usemap instead'
        );
        $this->registerTagAttribute(
            'longdesc',
            'string',
            'Specifies the URL to a document that contains a long description of an image'
        );
        $this->registerTagAttribute(
            'usemap',
            'string',
            'Specifies an image as a client-side image-map'
        );
        $this->registerArgument(
            'interchangeSettings',
            'array',
            'and change settings',
            false,
            []
        );
        $this->registerArgument(
            'falImage',
            FileReference::class,
            'Image Resource'
        );
        $this->registerArgument(
            'src',
            'string',
            'a path to a file, a combined FAL identifier or an uid (int). If $treatIdAsReference is set, the integer is considered the uid of the sys_file_reference record. If you already got a FAL object, consider using the $image parameter instead'
        );
        $this->registerArgument(
            'width',
            'string',
            'width of the image. This can be a numeric value representing the fixed width of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.'
            );
        $this->registerArgument(
            'height',
            'string',
            'height of the image. This can be a numeric value representing the fixed height of the image in pixels. But you can also perform simple calculations by adding "m" or "c" to the value. See imgResource.width for possible options.'
        );
        $this->registerArgument(
            'minWidth',
            'int',
            'minimum width of the image'
        );
        $this->registerArgument(
            'maxWidth',
            'int',
            'maximum width of the image'
        );
        $this->registerArgument(
            'minHeight',
            'int',
            'minimum height of the image'
        );
        $this->registerArgument(
            'maxHeight',
            'int',
            ' maximum height of the image'
        );
        $this->registerArgument(
            'treatIdAsReferenc',
            'bool',
            'given src argument is a sys_file_reference record'
        );
        $this->registerArgument(
            'crop',
            'bool|string',
            'overrule cropping of image (setting to FALSE disables the cropping set in FileReference)'
        );
        $this->registerArgument(
            'absolute',
            'bool',
            'Force absolute URL'
        );
    }

    /**
     * Resizes a given image (if required) and renders the respective img tag
     *
     * @see http://typo3.org/documentation/document-library/references/doc_core_tsref/4.2.0/view/1/5/#id4164427
     *
     * (integer). If $treatIdAsReference is set, the integer is considered the uid
     * of the sys_file_reference record. If you already got a FAL object, consider
     * using the $image parameter instead
     *
     * @throws Exception
     *
     * Example
     *     <my:imageInterchange
     *         image="{image}"
     *         interchangeSettings="{small: {width: 400}, medium: {width: 800}, default: {width: 1200}}"
     *     />
     *
     * @return string Rendered tag
     */

    public function render(): string
    {
        $src = $this->arguments['src'];
        $crop = $this->arguments['crop'];
        try {
            if ((($src === null) && ($this->arguments['falImage'] === null)) || (($src !== null) && ($this->arguments['falImage'] !== null))) {
                throw new InvalidArgumentException(
                    'You must either specify a string src xor a File object.',
                    1382284106
                );
            }
            if (is_bool($this->arguments['falImage'])) {
                throw new InvalidArgumentException('image attribute contains boolean, should be null or FileInterface');
            }
            /** @var array $interchangeSettings */
            $interchangeSettings = $this->arguments['interchangeSettings'];
            $image = $this->imageService->getImage(
                $src ?? '',
                $this->arguments['falImage'],
                $this->arguments['treatIdAsReference'] ?? true
            );
            if ($crop === null) {
                $crop = $image instanceof FileReference ? $image->getProperty('crop') : null;
            }

            $interchangeData = [];
            foreach ($interchangeSettings as $key => $settings) {
                $cropString = $settings['crop'] ?? '';
                if ($cropString === null && $image->hasProperty('crop') && $image->getProperty('crop')) {
                    $cropString = $image->getProperty('crop');
                }
                $cropVariantCollection = CropVariantCollection::create((string)$cropString);
                $cropVariant = $this->arguments['cropVariant'] ?? 'default';
                $cropArea = $cropVariantCollection->getCropArea($cropVariant);
                $processingInstructions = array(
                    'width' => $settings['width'] ?? $this->arguments['width'],
                    'height' => $settings['height'] ?? $this->arguments['height'],
                    'minWidth' => $settings['minWidth'] ?? $this->arguments['minWidth'],
                    'minHeight' => $settings['minHeight'] ?? $this->arguments['minHeight'],
                    'maxWidth' => $settings['maxWidth'] ?? $this->arguments['maxWidth'],
                    'maxHeight' => $settings['maxHeight'] ?? $this->arguments['maxHeight'],
                    'crop' =>$cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
                );
                $processedImage = $this->imageService->applyProcessingInstructions($image, $processingInstructions);
                $imageUri = $this->imageService->getImageUri($processedImage);

                $interchangeData[] = '[' . $imageUri . ', (' . $key . ')]';
            }

            if ($processedImage) {
                $this->tag->addAttribute(
                    'intrinsicsize',
                    $processedImage->getProperty('width') . ' x ' . $processedImage->getProperty('height')
                );
                $this->tag->addAttribute(
                    'width',
                    $processedImage->getProperty('width')
                );
            }

            $this->tag->addAttribute(
                'data-interchange',
                implode(',', $interchangeData)
            );
            $this->tag->addAttribute(
                'alt',
                $this->arguments['alt'] ?? $image->getProperty('alternative')
            );
            $this->tag->addAttribute(
                'title',
                $this->arguments['title'] ?? $image->getProperty('title')
            );
        } catch (ResourceDoesNotExistException $e) {
            // thrown if file does not exist
            $this->tag->addAttribute('data-error', 'resource does not exist');
        } catch (UnexpectedValueException $e) {
            // thrown if a file has been replaced with a folder
            $this->tag->addAttribute('data-error', 'resource is not a file, but a folder does not exist');
        } catch (InvalidArgumentException $e) {
            // thrown if file storage does not exist
            $this->tag->addAttribute('data-error', 'file storage does not exist');
        } catch (Exception $e) {
            $this->tag->addAttribute('data-error', 'viewHelper exception');
        } catch (RuntimeException $e) {
            // RuntimeException thrown if a file is outside of a storage
            $this->tag->addAttribute('data-error', 'resource is outside of storage does not exist');
        }



        $this->tag->addAttribute(
            'loading',
            'lazy'
        );

        return $this->tag->render();
    }
}
