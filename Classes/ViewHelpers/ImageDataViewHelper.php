<?php
namespace Drk\DrkTemplate\ViewHelpers;

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class ImageDataViewHelper extends AbstractViewHelper
{
    /**
     * output will not be escaped (for returnVal: 'htmlWidthHeight')
     */
    protected $escapeOutput = false;

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('imgPath', 'string', 'The path to the image, usually given as {imageRefField.originalResource.publicUrl}', false, null);
        $this->registerArgument('returnVal', 'string', 'One of the return values; if null, array will be returned', false, null);
    }

    /**
     * @return string
     */
    public function render(): string
    {
        if ($this->arguments['imgPath']) {
            return '';
        }
        $serverName = ($_SERVER['SERVER_NAME'] !== '127.0.0.1') ? $_SERVER['SERVER_NAME'] : 'www.drk.de';
        $protocol = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? 'https';
        $imgPath = $protocol . '://' . $serverName . $this->arguments['imgPath'];
        $fileResponseHeader = @get_headers($imgPath);
        if ($imgPath !== null && $fileResponseHeader[0] == 'HTTP/1.1 200 OK') {
            $dim = getimagesize($imgPath);
            if($dim !== false){
                return ' ' . $dim[3];
            } else {
                return '';
            }
        }
        return '';
    }
}
