<?php

namespace Drk\DrkTemplate\ViewHelpers;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2015 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * can be used to access array keys or object properties dynamically
 *
 * {themes:arrayIndex(object: results, index: 'key')}
 */
class ArrayIndexViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('object', 'mixed', 'object', true);
        $this->registerArgument('index', 'index', 'index', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $object = $arguments['object'];
        $index = $arguments['index'];

        if (is_object($object) && property_exists($object, $index)) {
            return $object->$index;
        }
        if (is_array($object) && isset($object[$index])) {
            return $object[$index];
        }
        return null;
    }
}
