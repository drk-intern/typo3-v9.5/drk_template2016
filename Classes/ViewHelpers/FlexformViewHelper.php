<?php

namespace Drk\DrkTemplate\ViewHelpers;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * can be used to access flexform keys
 *
 * {themes:flexform(object: results, index: 'key')}
 */
class FlexformViewHelper extends \TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper
{
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('object', 'mixed', 'object', true);
        $this->registerArgument('index', 'index', 'index', true);
    }

    public static function renderStatic(
        array $arguments,
        \Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ) {
        $object = $arguments['object'];
        $index = $arguments['index'];

        $oFlexformService = GeneralUtility::makeInstance(FlexFormService::class);

        $aFlexform = null;

        if (isset($object['pi_flexform']) && is_array($object['pi_flexform'])) {
            return $aFlexform[$index] ?? null;
        }

        if (is_object($object) && property_exists($object, 'pi_flexform')) {
            $aFlexform = $oFlexformService->convertFlexFormContentToArray($object->pi_flexform);
        }
        if ($aFlexform === null && isset($object['pi_flexform'])) {
            $aFlexform = $oFlexformService->convertFlexFormContentToArray($object['pi_flexform']);
        }
        return $aFlexform[$index] ?? null;
    }
}
