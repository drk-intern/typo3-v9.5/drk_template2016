<?php

namespace Drk\DrkTemplate\Domain\Model;

/**
 * Class News
 *
 * @notice do not edit code format
 * @package Drk\DrkTemplate\Domain\Model
 */
class News extends \GeorgRinger\News\Domain\Model\News
{

    /**
     * @var int
     */
    protected $isEvent;

    /**
     * @var int
     */
    protected $datetimeEnd;

    /**
     * @return int
     */
    public function getIsEvent()
    {
        return $this->isEvent;
    }

    /**
     * @param int $isEvent
     */
    public function setIsEvent($isEvent)
    {
        $this->isEvent = $isEvent;
    }

    /**
     * @return int
     */
    public function getDatetimeEnd()
    {
        return $this->datetimeEnd;
    }

    /**
     * @param int $datetimeEnd
     */
    public function setDatetimeEnd($datetimeEnd)
    {
        $this->datetimeEnd = $datetimeEnd;
    }
}
