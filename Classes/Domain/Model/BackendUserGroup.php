<?php
/**
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 * @author Kevin Purrmann
 * @description BackendUserGroup Model
 **/

namespace Drk\DrkTemplate\Domain\Model;

use TYPO3\CMS\Extbase\Domain\Model\BackendUserGroup as BaseBackendUserGroup;

class BackendUserGroup extends BaseBackendUserGroup
{
    /**
     * List of category uids
     * @var string
     */
    protected $categoryPerms;

    /**
     * @return string
     */
    public function getCategoryPerms()
    {
        return $this->categoryPerms;
    }

    /**
     * @param string $categoryPerms
     */
    public function setCategoryPerms($categoryPerms)
    {
        $this->categoryPerms = $categoryPerms;
    }
}
