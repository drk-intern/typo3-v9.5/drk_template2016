<?php
/**
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 * @author Kevin Purrmann
 * @description BackendGroupRepository
 **/

namespace Drk\DrkTemplate\Domain\Repository;

use TYPO3\CMS\Extbase\Domain\Repository\BackendUserGroupRepository as BaseBackendUserGroupRepository;

class BackendUserGroupRepository extends BaseBackendUserGroupRepository
{
}
